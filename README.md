# README #

The "Monte-Carlo with AccelerateHS" project is an attempt to implement and test a simple Monte-Carlo example using the AccelerateHS framework. The project is written as literate Haskell, with many comments and explanation in the code.

### CONTENTS ###
* BlackScholes.lhs -- Black-Scholes formula. 
* Main.lhs -- A particular MC example. The backend can be set to interpreter in order to run on CPU instead of GPU. 
* MatrixVector.lhs -- Data types and operations to model and update the state of the random number generator, and to extract value from the state.
* MonteCarloExample.lhs -- A simple MC example that generates stock price trajectories and estimates the expected payoff. 
* Mrg32k3a.lhs -- An implementation of the Mrg32k3a random number generator and a few distributions. 
* TestMrg32k3a.lhs -- A couple of unit tests.
* Unicode.lhs -- Helper functions for the unicode mode; in the style of Ralf Hinze.